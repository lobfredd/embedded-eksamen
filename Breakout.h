#ifndef BREAKOUT
#define BREAKOUT

#include "Game.h"
#define blocksSize 90

const SpriteLines brickSprite[2] PROGMEM = {
  {1, {{10, ST7735_WHITE} }},
  {1, {{10, ST7735_WHITE} }}
};
const SpriteLines ballSprite[4] PROGMEM = {
  {1, {{4, ST7735_WHITE} }},
  {1, {{4 , ST7735_WHITE} }},
  {1, {{4 , ST7735_WHITE} }},
  {1, {{4 , ST7735_WHITE} }}
};

const char scoreFileNames[5][8] PROGMEM = {"scr.ez", "scr.md", "scr.hrd", "scr.ex", "scr.hoe"};
const char* const scoreFileNamesP[] PROGMEM = {scoreFileNames[0], scoreFileNames[1], scoreFileNames[2], scoreFileNames[3], scoreFileNames[4]};

class Breakout : Game{
  private:
    GameObject* blocks;
    MovingGameObject player;
    MovingGameObject ball;
    uint8_t extraLives;
    uint8_t ballSpeedRegulator;
    uint8_t difficulty;
    unsigned long lastBrickHit;

  public:
    Breakout(Adafruit_ST7735* tft, Input* input);
    void startGame();
    
    private:
      bool gameLoop();
      void changeDifficulty();
      void initBlocks();
      bool movePlayer();
      void moveBall(bool playerMoved);
      void handleBallHitBlock();
      void handleShootableBall(bool playerMoved);
      uint8_t speedFactor(int value);
      bool playerShouldBeMovedLeft(int input);
      bool playerShouldBeMovedRight(int input);
      void showHighScores();
      void gameOver();
};

#endif
