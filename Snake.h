#ifndef SNAKE
#define SNAKE

#include "Game.h"
#define MOVEMENT_DIST 2
#define SNAKE_LENGTH 100
#define SPRITE_SIZE 4
#define BG_COLOR ST7735_BLACK

const SpriteLines snakeSprite[SPRITE_SIZE] PROGMEM = {
  {2, {{3, ST7735_GREEN}, {1, ST7735_BLACK} }},
  {4, {{1, ST7735_BLACK}, {1, ST7735_GREEN}, {1, ST7735_BLACK}, {1, ST7735_GREEN} }},
  {3, {{1, ST7735_GREEN}, {1, ST7735_BLACK}, {2, ST7735_GREEN} }},
  {2, {{1, ST7735_BLACK}, {3, ST7735_GREEN} }}
};

const SpriteLines foodSprite[SPRITE_SIZE] PROGMEM = {
  {3, {{1, BG_COLOR}, {2, ST7735_RED}, {1, BG_COLOR} }},
  {1, {{4, ST7735_RED} }},
  {1, {{4, ST7735_RED} }},
  {3, {{1, BG_COLOR}, {2, ST7735_RED}, {1, BG_COLOR} }}
};

class Snake : Game{
  private:
    GameObject* snake;
    GameObject food;
    uint8_t direction;
    unsigned long lastFrameTime;
    bool userBtnDown;

  public:
    Snake(Adafruit_ST7735* tft, Input* input);
    void startGame();
    
  private:
    bool gameLoop();
    void gameOver();
    void showHighScores();
    void spawnFood();
    void initSnake();
    void drawSnake();
    void moveSnake();
    void collidingWithFood();
    bool collidingWithWall();
    bool collidingWithSelf();
    void handleUserInput();
};

#endif

