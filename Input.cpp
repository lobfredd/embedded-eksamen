#include "Input.h"

Input::Input(uint8_t analogXPin, uint8_t analogYPin, uint8_t selectPin, uint8_t menuPin):
  analogXPin(analogXPin),
  analogYPin(analogYPin),
  selectPin(selectPin),
  menuPin(menuPin){
}

int Input::XAxis(){
  return analogRead(analogXPin);
}

int Input::YAxis(){
  return analogRead(analogYPin);
}

bool Input::selectBtnDown(){
  return digitalRead(selectPin) == LOW;
}

bool Input::menuBtnDown(){
  return digitalRead(menuPin) == LOW;
}

