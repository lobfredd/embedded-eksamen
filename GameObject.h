#ifndef GAMEOBJECT
#define GAMEOBJECT
#include <Arduino.h>

typedef struct row{
  uint8_t thickness;
  uint16_t color;
}Row;

typedef struct spriteLines{
  uint8_t linesPerRow;
  Row row[10];
}SpriteLines;

class GameObject{
  public:
    int x;
    int y;
    bool isActive;
    uint8_t width;
    uint8_t height;

    GameObject();
    GameObject(int x, int y, uint8_t width, uint8_t height);
    bool isColliding(GameObject* other);
};

#endif

