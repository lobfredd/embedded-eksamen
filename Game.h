#ifndef GAME
#define GAME

#include <Adafruit_ST7735.h> // Hardware-specific library
#include <SD.h>
#include <NewTone.h>
#include "GameObject.h"
#include "MovingGameObject.h"
#include "Input.h"
#include "pitches.h"
#define SOUND_PIN 3

class Game{
  protected:
    Adafruit_ST7735* tft;
    uint16_t colors[7] = {ST7735_BLUE, ST7735_RED, ST7735_GREEN, ST7735_CYAN, ST7735_MAGENTA, ST7735_YELLOW, ST7735_WHITE};
    uint8_t tftWidth;
    uint8_t tftHeight;
    uint16_t bg_color;
    uint16_t score;
    Input* input;

  public:
    Game(Adafruit_ST7735* tft, Input* input, uint16_t bg_color = ST7735_BLACK);
    int printMenuFileReturnSelection(char* filename);
    
  protected:
    void moveAndPrintGameObject(MovingGameObject* obj);
    void printGameObject(GameObject* obj, const SpriteLines* sprite, uint8_t spriteLength, uint32_t colorOverride = 0xFFFFFFFF);
    void removeObjectFromScreen(GameObject* obj);
  
    void setRotation(uint8_t newRotation);
    void reDrawTextAt(uint8_t x, uint8_t y, char* oldValue, char* newValue);
    void printText(int x, int y, char* value, uint16_t color);
    
    void saveScoreToTop3(uint16_t score, char* filename);
    void getTop3HighScores(int top3[], char* filename);
    void printTop3(char* fileName);
    int printTitle(char* title);
    void printGameOver(uint16_t score);
    int getHPos(char* text, uint8_t textSize);
    void printScore(uint16_t score, uint8_t y, uint8_t size, uint16_t color);
    
  private:
    int printMenuFile(char* filename, int hPos[], int* itemCount);
    void moveMenuCursor(uint8_t oldHPos, uint8_t newHPos, uint8_t oldVPos, uint8_t newVPos);
    int moveSelection(bool up, uint8_t startPos, uint8_t currentPos, int hPos[]);
    int getItemIndex(uint8_t currentPos, uint8_t startPos);
    void getNextLine(File file, char buf[]);
    void sort(int arr[], uint8_t count);
    void clearPrevLocation(MovingGameObject* obj, int8_t moveDistance, int axis, bool horizontalMove);

};


#endif
