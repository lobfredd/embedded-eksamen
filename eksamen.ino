#include <Adafruit_GFX.h>
#include <Adafruit_ST7735.h>
#include <SPI.h>
#include <SD.h>
#include <NewTone.h>
#include "Breakout.h"
#include "FlappyBird.h"
#include "Snake.h"
#include "Input.h"

#define TFT_CS 10
#define TFT_RST 8
#define TFT_DC 9
#define ANALOG_X_PIN A5
#define ANALOG_Y_PIN A4
#define SELECT_BTN_PIN 7
#define MENU_BTN_PIN 6
#define SD_CS_PIN 4
#define SOUND_PIN 3

Adafruit_ST7735 tft(TFT_CS, TFT_DC, TFT_RST);
Input input(ANALOG_X_PIN, ANALOG_Y_PIN, SELECT_BTN_PIN, MENU_BTN_PIN);

void setup() {
  pinMode(SELECT_BTN_PIN, INPUT_PULLUP);
  pinMode(MENU_BTN_PIN, INPUT_PULLUP);
  SD.begin(SD_CS_PIN);
  tft.initR(INITR_BLACKTAB);
}

void loop() {
  int result = showMainMenu();
  if(result == 0) startFlappy();
  else if(result == 1) startBreakout();
  else if(result == 2) startSnake();
  else if(result == 3) startMazeLap();
  else if(result == -1) delay(10000);
}

int showMainMenu(){
  Game g(&tft, &input);
  return g.printMenuFileReturnSelection("mn_main.txt");
}

void startFlappy(){
  FlappyBird f(&tft, &input);
  f.startGame();
}

void startBreakout(){
  Breakout b(&tft, &input);
  b.startGame();
}

void startSnake(){
  Snake s(&tft, &input);
  s.startGame();
}

void startMazeLap(){
  Game g(&tft, &input);
  g.printMenuFileReturnSelection("error404.txt");
  delay(1000);
}

