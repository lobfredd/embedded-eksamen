#include "Breakout.h"

Breakout::Breakout(Adafruit_ST7735* tft, Input* input) : 
  Game(tft, input),
  player(50, 100, 1, 0, brickSprite, 2), 
  ball(54, 94, 1, 0, ballSprite, 4),
  ballSpeedRegulator(10),
  difficulty(0),
  lastBrickHit(0){

  player.y = tftHeight-30;
  ball.y = player.y-7;
  ball.isActive = false;
}

void Breakout::startGame(){
  while(true){
    int result = printMenuFileReturnSelection("br_main.txt");
    if(result == 0 && gameLoop()) gameOver(); //if gameLoop return true == gameover, to clear stack before showing gameover screen
    else if(result == 1) changeDifficulty();
    else if(result == 2) showHighScores();
    else if(result == 3) return;
  } 
}

void Breakout::showHighScores(){
  int result = printMenuFileReturnSelection("br_hscr.txt");
  if(result == -1) return;
  char fileName[8];
  strcpy_P(fileName, (char*)pgm_read_word(&(scoreFileNamesP[result])));
  printTop3(fileName);
  while(!input->menuBtnDown());
}

bool Breakout::gameLoop(){
  tft->fillScreen(ST7735_BLACK);
  GameObject blockz[blocksSize];
  blocks = blockz;
  initBlocks();
  extraLives = 3;
  score = 0;
  moveAndPrintGameObject(&ball);
  moveAndPrintGameObject(&player);
  
  while(true){
    printScore(score, 0, 1, ST7735_WHITE);
    bool playerMoved = movePlayer();
    if(ball.isActive) moveBall(playerMoved);
    else if(extraLives > 0 && score < blocksSize) handleShootableBall(playerMoved);
    else return true;
    if(input->menuBtnDown()) return false;
  }
}

void Breakout::gameOver(){
  printGameOver(score);
  
  char fileName[8];
  strcpy_P(fileName, (char*)pgm_read_word(&(scoreFileNamesP[difficulty])));
  saveScoreToTop3(score, fileName);
  while(!input->menuBtnDown() && !input->selectBtnDown());
}

void Breakout::changeDifficulty(){
  int result = printMenuFileReturnSelection("br_dif.txt");
  if(result == -1) return;
  difficulty = result;
  ballSpeedRegulator = 10-(result*2);
}

void Breakout::initBlocks(){
  randomSeed(analogRead(0));
  int startX = 3;
  int startY = 10;
  for(int i = 0; i < blocksSize; i++){
    blocks[i] = GameObject(startX, startY, 10, 2);
    startX += 12;
    if(startX >= tftWidth-3){
      startX = 3;
      startY += 5;
    }
    printGameObject(&blocks[i], brickSprite, 2, colors[random(0, 7)]);
  }
}

bool Breakout::movePlayer(){
  int xAxis = input->XAxis();
  
  if(millis() - player.lastMoveTime > speedFactor(xAxis)){
    if(playerShouldBeMovedLeft(xAxis)) player.xVel = -2;
    else if(playerShouldBeMovedRight(xAxis)) player.xVel = 2;
    else return false; //no valid input requested
    
    moveAndPrintGameObject(&player);
    return true;
  }
  return false;
}

void Breakout::moveBall(bool playerMoved){
  if(millis() - ball.lastMoveTime > ballSpeedRegulator){
    ball.constrainObj(tftWidth, tftHeight+40); //keeping it on screen, but the bottom
    
    if(ball.isColliding(&player)){
      ball.yVel = -1; //turning, player can only push ball up
      if(playerMoved) ball.xVel = (player.xVel > 0) ? 1 : -1; 
    }
    else if(ball.y >= tftHeight){ //hit bottom, respawn
      extraLives--;
      ball = MovingGameObject(player.x+3, player.y-7, 1, 0, ballSprite, 4);
      ball.isActive = false;
    }
    else handleBallHitBlock();
    
    moveAndPrintGameObject(&ball);
  }
}

void Breakout::handleBallHitBlock(){
  for(int i = 0; i < blocksSize; i++){  
    if(ball.isColliding(&blocks[i]) && blocks[i].isActive && (millis() - lastBrickHit > 200)){
      ball.yVel = -ball.yVel; //turning
      removeObjectFromScreen(&blocks[i]);
      NewTone(SOUND_PIN, NOTE_A7, 1000 / 16);
      blocks[i].isActive = false;
      printScore(score, 0, 1, bg_color);
      score++;
      lastBrickHit = millis();
    }
  }
}

void Breakout::handleShootableBall(bool playerMoved){
  if(input->selectBtnDown()){
    ball.xVel = (player.xVel > 0) ? 1 : -1;
    ball.yVel = -1;
    ball.isActive = true;
  }
  else if(playerMoved){
    ball.xVel = player.xVel;
    ball.yVel = player.yVel;
    moveAndPrintGameObject(&ball);
  }
}

//Returns a number between 5 and 50, depending on the distance from 512
uint8_t Breakout::speedFactor(int value){
  int speed = 512 - value;
  speed = constrain(abs(speed), 0, 512);
  speed = map(speed, 0, 512, 0, 45);
  speed = 50 - speed;
  return speed;
}

bool Breakout::playerShouldBeMovedLeft(int input){
  return input < 500 && player.x > 1;
}

bool Breakout::playerShouldBeMovedRight(int input){
  return input > 524 && player.x < tftWidth-player.width;
}

