#ifndef FLAPPYBIRD
#define FLAPPYBIRD

#define PIPE_GAP 35
#define BG_COLOR 0x7BE
#define BOUNCE_FORCE -3
#define GRAVITY_FORCE 1
#define BIRD_BOUNCE_TIME 200
#define PIPE_WIDTH 20
#include "Game.h"

typedef struct pipe{
  int x;
  bool pointingDown;
  uint8_t height;
}Pipe;

const SpriteLines birdSpriteWingsUp[12] PROGMEM = {
  {3, {{6, BG_COLOR}, {6, ST7735_BLACK}, {5, BG_COLOR} }},
  {7, {{4, BG_COLOR}, {2, ST7735_BLACK}, {3, ST7735_WHITE}, {1, ST7735_BLACK}, {2, ST7735_WHITE}, {1, ST7735_BLACK}, {4, BG_COLOR} }},
  {8, {{3, BG_COLOR},{1, ST7735_BLACK}, {2, ST7735_WHITE}, {2, ST7735_YELLOW}, {1, ST7735_BLACK}, {4, ST7735_WHITE}, {1, ST7735_BLACK}, {3, BG_COLOR} }},
  {9, {{1, BG_COLOR}, {4,  ST7735_BLACK}, {3, ST7735_YELLOW}, {1, ST7735_BLACK}, {3, ST7735_WHITE}, {1, ST7735_BLACK}, {1,ST7735_WHITE}, {1, ST7735_BLACK}, {2, BG_COLOR} }},
  {10, {{1, ST7735_BLACK}, {4, ST7735_WHITE}, {1, ST7735_BLACK}, {2, ST7735_YELLOW}, {1, ST7735_BLACK}, {3, ST7735_WHITE}, {1, ST7735_BLACK}, {1,ST7735_WHITE}, {1, ST7735_BLACK}, {2, BG_COLOR} }},
  {8, {{1, ST7735_BLACK}, {5, ST7735_WHITE}, {1, ST7735_BLACK}, {2, ST7735_YELLOW}, {1, ST7735_BLACK}, {4, ST7735_WHITE}, {1, ST7735_BLACK}, {2, BG_COLOR} }},
  {8, {{1, ST7735_BLACK}, {1, ST7735_YELLOW}, {3, ST7735_WHITE}, {1, ST7735_YELLOW}, {1, ST7735_BLACK}, {3, ST7735_YELLOW}, {6,ST7735_BLACK}, {1, BG_COLOR} }},
  {8, {{1, BG_COLOR}, {1, ST7735_BLACK}, {3, ST7735_YELLOW}, {1, ST7735_BLACK}, {3, ST7735_YELLOW}, {1, ST7735_BLACK}, {6, ST7735_RED}, {1, ST7735_BLACK} }},
  {7, {{2, BG_COLOR}, {3, ST7735_BLACK}, {3, 0xFE00}, {1, ST7735_BLACK}, {1, ST7735_RED}, {6, ST7735_BLACK},{1, BG_COLOR} }},
  {7, {{2, BG_COLOR}, {1, ST7735_BLACK}, {6, 0xFE00}, {1, ST7735_BLACK}, {5, ST7735_RED}, {1, ST7735_BLACK}, {1, BG_COLOR} }},
  {5, {{3, BG_COLOR}, {2, ST7735_BLACK}, {5, 0xFE00}, {5, ST7735_BLACK}, {2, BG_COLOR} }},
  {3, {{5, BG_COLOR}, {5, ST7735_BLACK}, {7, BG_COLOR} }}
};

const SpriteLines birdSpriteWingsDown[12] PROGMEM = {
  {3, {{6, BG_COLOR}, {6, ST7735_BLACK}, {5, BG_COLOR} }},
  {7, {{4, BG_COLOR}, {2, ST7735_BLACK}, {3, ST7735_WHITE}, {1, ST7735_BLACK}, {2, ST7735_WHITE}, {1, ST7735_BLACK}, {4, BG_COLOR} }},
  {8, {{3, BG_COLOR},{1, ST7735_BLACK}, {2, ST7735_WHITE}, {2, ST7735_YELLOW}, {1, ST7735_BLACK}, {4, ST7735_WHITE}, {1, ST7735_BLACK}, {3, BG_COLOR} }},
  {10, {{2, BG_COLOR}, {1, ST7735_BLACK},{1, ST7735_WHITE}, {4, ST7735_YELLOW}, {1, ST7735_BLACK}, {3, ST7735_WHITE}, {1, ST7735_BLACK}, {1,ST7735_WHITE}, {1, ST7735_BLACK}, {2, BG_COLOR} }},
  {9, {{1, BG_COLOR},{1, ST7735_BLACK}, {6, ST7735_YELLOW}, {1, ST7735_BLACK}, {3, ST7735_WHITE}, {1, ST7735_BLACK}, {1,ST7735_WHITE}, {1, ST7735_BLACK}, {2, BG_COLOR} }},
  {7, {{1, BG_COLOR},{1, ST7735_BLACK}, {7, ST7735_YELLOW}, {1, ST7735_BLACK}, {4, ST7735_WHITE}, {1, ST7735_BLACK}, {2, BG_COLOR} }},
  {5, {{1, BG_COLOR},{5, ST7735_BLACK}, {4, ST7735_YELLOW}, {6, ST7735_BLACK}, {1, BG_COLOR} }},
  {7, {{1, ST7735_BLACK}, {5, ST7735_YELLOW}, {1, ST7735_BLACK}, {2, ST7735_YELLOW}, {1, ST7735_BLACK}, {6, ST7735_RED}, {1, ST7735_BLACK} }},
  {8, {{1, ST7735_BLACK}, {4, ST7735_YELLOW}, {1, ST7735_BLACK}, {2, 0xFE00}, {1, ST7735_BLACK}, {1, ST7735_RED}, {6, ST7735_BLACK}, {1, BG_COLOR} }},
  {8, {{1, ST7735_BLACK}, {3, ST7735_YELLOW}, {1, ST7735_BLACK}, {4, 0xFE00}, {1, ST7735_BLACK}, {5, ST7735_RED}, {1, ST7735_BLACK}, {1, BG_COLOR} }},
  {5, {{1, BG_COLOR}, {4, ST7735_BLACK}, {5, 0xFE00}, {5, ST7735_BLACK}, {2, BG_COLOR} }},
  {3, {{5, BG_COLOR}, {5, ST7735_BLACK}, {7, BG_COLOR} }}
};

const Row pipeSprite[4] PROGMEM = {{1, ST7735_BLACK},{5, ST7735_WHITE}, {13, ST7735_GREEN}, {1, ST7735_BLACK}};
const Row footerSprite[6] PROGMEM = {{1, ST7735_BLACK},{2, 0xE7F1}, {12, 0x9F2B}, {1, 0x5404}, {2, 0xD549}, {8, 0xDED2}};

class FlappyBird : Game{
  private:
    uint8_t footerHeight;
    MovingGameObject bird;
    Pipe p1;
    Pipe p2;
    unsigned long lastBirdBounce;
    unsigned long lastPipeMove;
    bool gotPointCurrentPipeRound;
    bool userBtnDown;

  public:
    FlappyBird(Adafruit_ST7735* tft, Input* input);
    void startGame();
    
  private:
    void gameLoop();
    void movePipe(Pipe* pipe);
    void drawFooter();
    void respawnPipesIfOutOfScreen();
    void calculateFooterHeight();
    void movePipes();
    void moveBird();
    bool birdBounceTimeExpired();
    bool birdIsBouncing();
    bool birdIsNotAboveScreen();
    bool birdIsNotBelowFooter();
    bool birdPipeCollision();
    bool birdCollidingWith(Pipe* p);
    bool birdPassedPipes();
    void gameOver();
    void showHighScores();
};

#endif

