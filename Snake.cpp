#include "Snake.h"

Snake::Snake(Adafruit_ST7735* tft, Input* input) : 
    Game(tft, input, BG_COLOR),
    lastFrameTime(0),
    food(0,0,1,1){
    randomSeed(analogRead(0));
}

void Snake::startGame(){
  while(true){
    int result = printMenuFileReturnSelection("sk_main.txt");
    if(result == 0 && gameLoop()) gameOver(); //if gameLoop return true == gameover, to clear stack before showing gameover screen
    else if(result == 1) showHighScores();
    else if(result == 2) return;
  }
}

bool Snake::gameLoop(){
  GameObject sn[SNAKE_LENGTH];
  snake = sn;
  tft->fillScreen(bg_color);
  initSnake();
  spawnFood();
  userBtnDown = true;
  direction = 0;
  score = 0;
  
  while(true){
    if(millis() - lastFrameTime < 30) continue;
    lastFrameTime = millis();

    printScore(score, 0, 1, ST7735_WHITE);
    moveSnake();
    printGameObject(&food, foodSprite, SPRITE_SIZE);
    collidingWithFood();
    handleUserInput();
    if(collidingWithWall() || collidingWithSelf() || snake[SNAKE_LENGTH-1].isActive) return true; //gameOver if snakelength is full
    else if(input->menuBtnDown()) return false;
  }
}

void Snake::handleUserInput(){
  if(!userBtnDown && input->selectBtnDown()){
    direction = (direction == 3) ? 0 : direction+1;
    userBtnDown = true;
  }
  else if(userBtnDown && !input->selectBtnDown()) userBtnDown = false;
}

void Snake::showHighScores(){
  printTop3("sk_hscr.txt");
  while(!input->menuBtnDown());
}

bool Snake::collidingWithSelf(){
  for(int i = 1; i < SNAKE_LENGTH; i++){
    if(snake[i].isActive && 
      snake[0].x == snake[i].x && 
      snake[0].y == snake[i].y)
      return true;
  }
  return false;
}

bool Snake::collidingWithWall(){
  return snake[0].x < 0 || snake[0].x > tftWidth-snake[0].width || snake[0].y < 0 || snake[0].y > tftHeight-snake[0].height;
}

void Snake::collidingWithFood(){
  if(!snake[0].isColliding(&food)) return;
  
  printScore(score, 0, 1, bg_color); //removing last score
  NewTone(SOUND_PIN, NOTE_A7, 1000 / 16);
  score++;
  spawnFood();
  
  for(int i = 1; i < SNAKE_LENGTH; i++){ //make snake longer by activating next snake part
    if(!snake[i].isActive){
      snake[i].isActive = true;
      break;
    }
  }
}

void Snake::spawnFood(){
  removeObjectFromScreen(&food);
  food = GameObject(random(5, tftWidth-5), random(5, tftHeight-5), SPRITE_SIZE, SPRITE_SIZE);
}

void Snake::initSnake(){
  snake[0] = GameObject(20, 20, SPRITE_SIZE, SPRITE_SIZE);
  
  for(int i = 1; i < SNAKE_LENGTH; i++){
    snake[i] = GameObject(snake[i-1].x-MOVEMENT_DIST, snake[i-1].y, SPRITE_SIZE, SPRITE_SIZE);
    snake[i].isActive = false;
  }
  for(int i = 0; i < 5; i++) snake[i].isActive = true; //activating 5 parts
}

void Snake::moveSnake(){
  GameObject p;
  p.isActive = false;
  for(int i = SNAKE_LENGTH-1; i > 0; i--){
    snake[i].x = snake[i-1].x;
    snake[i].y = snake[i-1].y;
    if(!p.isActive && snake[i].isActive) p = snake[i]; //grabbing first active part
  }
  
  if(direction == 0) snake[0].x += MOVEMENT_DIST;
  else if(direction == 1)snake[0].y += -MOVEMENT_DIST;
  else if(direction == 2)snake[0].x += -MOVEMENT_DIST;
  else if(direction == 3) snake[0].y += MOVEMENT_DIST;
 
  printGameObject(&snake[0], snakeSprite, SPRITE_SIZE);
  removeObjectFromScreen(&p);
}

void Snake::gameOver(){
  printGameOver(score);
  saveScoreToTop3(score, "sk_hscr.txt");
  while(!input->menuBtnDown() && !input->selectBtnDown());
}

