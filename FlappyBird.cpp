#include "FlappyBird.h"

FlappyBird::FlappyBird(Adafruit_ST7735* tft, Input* input) : 
    Game(tft, input, BG_COLOR),
    lastBirdBounce(0),
    lastPipeMove(0){
    calculateFooterHeight();
    randomSeed(analogRead(0));
}

void FlappyBird::startGame(){
  while(true){
    int result = printMenuFileReturnSelection("fp_main.txt");
    if(result == 0) gameLoop();
    else if(result == 1) showHighScores();
    else if(result == 2) return;
  }
}

void FlappyBird::showHighScores(){
  printTop3("fp_hscr.txt");
  while(!input->menuBtnDown());
}

void FlappyBird::gameLoop(){
  tft->fillScreen(BG_COLOR);
  drawFooter();
  bird = MovingGameObject(50, 50, 0, 1, birdSpriteWingsUp, 12);
  p1 = {-100, true, 15};
  p2 = {-100, false, 20};
  score = 0;
  userBtnDown = true;
  gotPointCurrentPipeRound = false;
  
  while(true){
    movePipes();
    moveBird();
    printScore(score, 10, 2, ST7735_WHITE);
    if(birdPipeCollision()) return gameOver();
    if(birdPassedPipes() && !gotPointCurrentPipeRound){
      printScore(score, 10, 2, BG_COLOR);
      score++;
      gotPointCurrentPipeRound = true;
    }
    if(input->menuBtnDown()) return;
  }
}

bool FlappyBird::birdPassedPipes(){
  return p1.x + PIPE_WIDTH <= bird.x;
}

void FlappyBird::movePipes(){
  if(millis() - lastPipeMove > 20){
    respawnPipesIfOutOfScreen();
    movePipe(&p1);
    movePipe(&p2);
    lastPipeMove = millis();
  }
}

void FlappyBird::moveBird(){
  if(userBtnDown && !input->selectBtnDown()) userBtnDown = false;
  if(input->selectBtnDown() && birdBounceTimeExpired() && birdIsNotAboveScreen() && !userBtnDown){
    bird.yVel = BOUNCE_FORCE;
    lastBirdBounce = millis();
    bird.sprite = (bird.sprite == birdSpriteWingsUp) ? birdSpriteWingsDown : birdSpriteWingsUp;
    NewTone(SOUND_PIN, NOTE_DS8, 1000 / 32);
    userBtnDown = true;
  }
  else if(birdBounceTimeExpired() && birdIsNotBelowFooter()){
    bird.sprite = birdSpriteWingsUp;
    bird.yVel = GRAVITY_FORCE;
  }
  else if(!birdIsBouncing()){ //if not bouncing, lock in position until bounce time expired
    bird.yVel = 0;
  }
  
  moveAndPrintGameObject(&bird);
}

void FlappyBird::gameOver(){
  printGameOver(score);
  saveScoreToTop3(score, "fp_hscr.txt");
  while(!input->menuBtnDown() && !input->selectBtnDown());
}

bool FlappyBird::birdPipeCollision(){
  return birdCollidingWith(&p1) || birdCollidingWith(&p2);
}

bool FlappyBird::birdCollidingWith(Pipe* p){
  int yLoc = (p->pointingDown) ? 0 : (tftHeight-footerHeight+1) - p->height;
  
  return ((bird.x+bird.width >= p->x && bird.x <= p->x + PIPE_WIDTH) && 
          (bird.y+bird.height >= yLoc && bird.y <= yLoc + p->height));
}

bool FlappyBird::birdBounceTimeExpired(){
  return millis() - lastBirdBounce > BIRD_BOUNCE_TIME;
}

bool FlappyBird::birdIsBouncing(){
  return millis() - lastBirdBounce < BIRD_BOUNCE_TIME - 100;
}

bool FlappyBird::birdIsNotAboveScreen(){
  return bird.y - BOUNCE_FORCE > 0;
}

bool FlappyBird::birdIsNotBelowFooter(){
  return bird.y + bird.height + GRAVITY_FORCE < tftHeight - footerHeight;
}

void FlappyBird::respawnPipesIfOutOfScreen(){
  if(p1.x < - PIPE_WIDTH){
    p1.height = random(12, (tftHeight - footerHeight) - PIPE_GAP - 12);
    p2.height = (tftHeight-footerHeight+1) - (p1.height + PIPE_GAP);
    p1.x = tftWidth;
    p2.x = tftWidth;
    gotPointCurrentPipeRound = false;
  }
}

void FlappyBird::movePipe(Pipe* pipe){
  int yLoc = (pipe->pointingDown) ? 0 : (tftHeight-footerHeight+1) - pipe->height;
  tft->drawFastVLine((pipe->x + PIPE_WIDTH), yLoc-1, pipe->height + 1, BG_COLOR);
  int x = pipe->x;
  for(int i = 0; i < 4; i++){
    uint8_t thickness = pgm_read_byte_near(&(pipeSprite+i)->thickness);
    uint16_t color = pgm_read_word_near(&(pipeSprite+i)->color);
    for(int j = 0; j < thickness; j++){
      if(x >= 0) tft->drawFastVLine(x++, yLoc, pipe->height, color);
      else x++; //not drawing out of screen lines
    }
  }
  if(pipe->pointingDown) yLoc = pipe->height-10;
  tft->drawFastHLine(pipe->x, yLoc, PIPE_WIDTH, ST7735_BLACK);
  tft->drawFastHLine(pipe->x, yLoc + 9, PIPE_WIDTH, ST7735_BLACK);
  pipe->x--;
}

void FlappyBird::drawFooter(){ 
  int y = tftHeight;
  for(int i = 5; i >= 0; i--){
    uint8_t thickness = pgm_read_byte_near(&(footerSprite+i)->thickness);
    uint16_t color = pgm_read_word_near(&(footerSprite+i)->color);
    for(int j = 0; j < thickness; j++){
      tft->drawFastHLine(0, y--, tftWidth, color);
    }
  }
}

void FlappyBird::calculateFooterHeight(){
  footerHeight = 0;
  for(int i = 0; i < 6; i++){
    footerHeight += pgm_read_byte_near(&(footerSprite+i)->thickness);
  }
}

