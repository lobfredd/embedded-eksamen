#include "GameObject.h"

GameObject::GameObject(){}

GameObject::GameObject(int x, int y, uint8_t width, uint8_t height): 
  x(x),
  y(y),
  width(width),
  height(height){
  isActive = true;
}

bool GameObject::isColliding(GameObject* other){
  return (x + width >= other->x && x <= other->x + other->width) &&
         (y + height >= other->y && y <= other->y + other->height);
}

