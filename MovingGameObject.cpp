#include "MovingGameObject.h"

MovingGameObject::MovingGameObject(){}

MovingGameObject::MovingGameObject(int x, int y, int8_t xVel, int8_t yVel, const SpriteLines* sprite, uint8_t spriteLength) : 
  GameObject(x, y, 0, spriteLength),
  xVel(xVel),
  yVel(yVel),
  sprite(sprite){
  lastMoveTime = 0;
  width = getSpriteRowLength(0);
}

void MovingGameObject::move(){
  x += xVel;
  y += yVel;
  lastMoveTime = millis();
}

void MovingGameObject::constrainObj(uint8_t tftWidth, uint8_t tftHeight){
  if(x-abs(xVel) < 0 || x >= tftWidth-width) xVel = -xVel;
  if(y-abs(yVel) < 0 || y >= tftHeight-height) yVel = -yVel;
}

uint8_t MovingGameObject::getSpriteRowLength(uint8_t theRow){
  uint8_t lineWidth = 0;
  uint8_t linesPerRow = pgm_read_byte_near(&(sprite+theRow)->linesPerRow);
  for(int j = 0; j < linesPerRow; j++){
    lineWidth += pgm_read_byte_near(&((sprite+theRow)->row+j)->thickness);
  }
  return lineWidth;
}


