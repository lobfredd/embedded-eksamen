#ifndef MOVINGGAMEOBJECT
#define MOVINGGAMEOBJECT
#include "GameObject.h"

class MovingGameObject : public GameObject{
  public:
    int8_t xVel;
    int8_t yVel;
    unsigned long lastMoveTime;
    const SpriteLines* sprite;

    MovingGameObject();
    MovingGameObject(int x, int y, int8_t xVel, int8_t yVel, const SpriteLines* sprite, uint8_t spriteLength);
    void move();
    void constrainObj(uint8_t tftWidth, uint8_t tftHeight);
    uint8_t getSpriteRowLength(uint8_t theRow);
};

#endif
