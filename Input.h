#ifndef INPUT_HEADER
#define INPUT_HEADER

#include <Arduino.h>

class Input{
  private:
    uint8_t analogXPin;
    uint8_t analogYPin;
    uint8_t selectPin;
    uint8_t menuPin;

  public:
    Input(uint8_t analogXPin, uint8_t analogYPin, uint8_t selectPin, uint8_t menuPin);
    int XAxis();
    int YAxis();
    bool selectBtnDown();
    bool menuBtnDown();
};

#endif
