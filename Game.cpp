#include "Game.h"

Game::Game(Adafruit_ST7735* tft, Input* input, uint16_t bg_color) : tft(tft), input(input), bg_color(bg_color){
  setRotation(3);
  tft->fillScreen(ST7735_BLACK);
}

void Game::moveAndPrintGameObject(MovingGameObject* obj){
  if(obj->xVel != 0) clearPrevLocation(obj, obj->xVel, obj->x, true);
  if(obj->yVel != 0) clearPrevLocation(obj, obj->yVel, obj->y, false);
  obj->move();
  printGameObject(obj, obj->sprite, obj->height);
}

void Game::clearPrevLocation(MovingGameObject* obj, int8_t moveDistance, int axis, bool horizontalMove){
  int mass = (horizontalMove) ? obj->width : obj->height;
  int startPos = (moveDistance > 0) ? axis : (axis + mass) + moveDistance;
  for(int i = 0; i < abs(moveDistance); i++){
    if(horizontalMove) tft->drawFastVLine(startPos+i, obj->y, obj->height, bg_color);
    else tft->drawFastHLine(obj->x, startPos+i, obj->width, bg_color);
  }
}

void Game::printGameObject(GameObject* obj, const SpriteLines* sprite, uint8_t spriteLength, uint32_t colorOverride){
  for(int i = 0; i < spriteLength; i++){
    char numOfLines =  pgm_read_byte_near(&(sprite + i)->linesPerRow);
    
    int offset = obj->x;
    for(int j = 0; j < numOfLines; j++){
      char thickness = pgm_read_byte_near(&((sprite+i)->row+j)->thickness);
      uint16_t color =  (colorOverride < 0xFFFFFFFF) ? colorOverride : pgm_read_word_near(&((sprite+i)->row+j)->color);
      tft->drawFastHLine(offset, obj->y+i, thickness, color);
      offset += thickness;
    }
  }
}

void Game::removeObjectFromScreen(GameObject* obj){
  tft->fillRect(obj->x, obj->y, obj->width, obj->height, ST7735_BLACK);
}

void Game::setRotation(uint8_t newRotation){
  tft->setRotation(newRotation);
  tftWidth = (newRotation == 1 || newRotation == 3) ? ST7735_TFTHEIGHT_18 : ST7735_TFTWIDTH;
  tftHeight = (newRotation == 1 || newRotation == 3) ? ST7735_TFTWIDTH : ST7735_TFTHEIGHT_18;
}

void Game::reDrawTextAt(uint8_t x, uint8_t y, char* oldValue, char* newValue){
  if(strlen(oldValue) > 0) printText(x, y, oldValue, ST7735_BLACK);
  if(strlen(newValue) > 0) printText(x, y, newValue, ST7735_WHITE);
}

void Game::printText(int x, int y, char* value, uint16_t color){
    tft->setTextColor(color);
    tft->setCursor(x, y);
    tft->print(value);
}

int Game::printMenuFileReturnSelection(char* filename){
  int hPos[10];
  int itemCount;
  int startPos = printMenuFile(filename, hPos, &itemCount);
  if(startPos == -1){
    printText(0, 0, "SD card error!,\n Menu not found..", ST7735_WHITE);
    return startPos;
  }
  int currentPos = startPos;
  long lastInput = 0;
  bool direction;
  moveMenuCursor(hPos[0], hPos[0], currentPos, currentPos);
  
  while(true){
    if(millis() - lastInput < 300) continue;
    else if(input->YAxis() < 500 && currentPos > startPos) direction = true;
    else if(input->YAxis() > 524 && currentPos < startPos+(10*(itemCount-1))) direction = false;
    else if(input->selectBtnDown()) return getItemIndex(currentPos, startPos);
    else if(input->menuBtnDown()) return -1;
    else continue;
    currentPos = moveSelection(direction, startPos, currentPos, hPos);
    lastInput = millis();
  }
}

int Game::printMenuFile(char* filename, int hPos[], int* itemCount){
  File file = SD.open(filename);
  if(!file) return -1;
  char buf[21];
  getNextLine(file, buf);
  int startPos = printTitle(buf) + 20;

  tft->setTextSize(1);
  *itemCount = 0;
  for(; *itemCount < 10 && file.available();){
    getNextLine(file, buf);
    hPos[*itemCount] = getHPos(buf, 1);
    printText(hPos[*itemCount], (10*(*itemCount))+startPos, buf, ST7735_WHITE);
    (*itemCount)++;
  }
  file.close();
  return startPos;
}

void Game::saveScoreToTop3(uint16_t score, char* filename){
  int top3[4];
  getTop3HighScores(top3, filename);
  top3[3] = score;
  sort(top3, 4);
  SD.remove(filename);
  File file = SD.open(filename, FILE_WRITE);
  if(!file) return;
  for(int i = 0; i < 3; i++) file.write((byte*)&top3[i], 2);
  file.close();
}

void Game::getTop3HighScores(int top3[], char* filename){
  File file = SD.open(filename);
  if(!file){
    for(int i = 0; i < 3; i++) top3[i] = 0;
    return;
  }
  for(int i = 0; i < 3; i++){
    top3[i] = (file.available()) ? file.read() : 0;
    top3[i] |= (file.available()) ? (file.read() << 8) : 0;
    if(top3[i] < 0 || top3[i] > 999) top3[i] = 0; //data corruption
  }
  file.close();
}

void Game::printTop3(char* fileName){
  int top3[3];
  getTop3HighScores(top3, fileName);
  printTitle("High Scores");
  tft->setTextSize(2);
  for(int i = 0; i < 3; i++){
    char txt[15];
    snprintf(txt, 15, "%d. %d", i+1, top3[i]);
    printText(50, 50+(20*i), txt, ST7735_WHITE);
  }
}

int Game::printTitle(char* title){
  tft->fillScreen(ST7735_BLACK);
  int txtSize = tftWidth / (6 * strlen(title));
  int hPos = getHPos(title, txtSize);
  tft->setTextSize(txtSize);
  printText(hPos, 10, title, ST7735_WHITE);
  return 7*txtSize;
}

void Game::moveMenuCursor(uint8_t oldHPos, uint8_t newHPos, uint8_t oldVPos, uint8_t newVPos){
  printText(oldHPos-10, oldVPos, "*" , ST7735_BLACK);
  printText(newHPos-10, newVPos, "*" , ST7735_WHITE);
}

int Game::moveSelection(bool up, uint8_t startPos, uint8_t currentPos, int hPos[]){
  int dirDist = (up) ? -10 : 10; 
  int i = getItemIndex(currentPos, startPos);
  int j = getItemIndex(currentPos+dirDist, startPos);
  moveMenuCursor(hPos[i], hPos[j], currentPos, currentPos+dirDist);
  return currentPos+dirDist;
}

int Game::getHPos(char* text, uint8_t txtSize){
  int width = 6 * strlen(text) * txtSize;
  return (tftWidth - width) / 2 + (txtSize/2);
}

int Game::getItemIndex(uint8_t currentPos, uint8_t startPos){
  return (currentPos-startPos)/10;
}

void Game::getNextLine(File file, char buf[]){
  char c;
  int i = 0;
  for(; i < 20 && file.available() && (c = file.read()) != '\n'; i++) buf[i] = c;
  buf[i] = '\0';
}

void Game::sort(int arr[], uint8_t count){
  for (int i = 1; i < count; i++) {
    for (int j = i; j > 0 && arr[j] > arr[j-1]; j--) {
      int temp = arr[j-1];
      arr[j-1] = arr[j];
      arr[j] = temp;
    }
  }
}

void Game::printGameOver(uint16_t score){
  int vPos = printTitle("Game Over!") + 50;
  char scoreTxt[20];
  snprintf(scoreTxt, 20, "Score: %d", score);
  
  int txtSize = tftWidth / (6 * strlen(scoreTxt));
  int hPos = getHPos(scoreTxt, txtSize);
  tft->setTextSize(txtSize);
  printText(hPos, vPos, scoreTxt, ST7735_WHITE);
}

void Game::printScore(uint16_t score, uint8_t y, uint8_t size, uint16_t color){
  char scoreTxt[4];
  snprintf(scoreTxt, 4, "%d", score);
  int hPos = getHPos(scoreTxt, size);
  tft->setTextSize(size);
  printText(hPos, y, scoreTxt, color);
}

